#!/bin/bash

#SBATCH --job-name=Stat
#SBATCH --cpus-per-task=8
#SBATCH --mem=64G

## Local environments
source environment.sh

## Activate R environment
conda deactivate
conda activate ./bio_r_env

## Prepare inputs for the regression analysis
cp $(ls -t ${OUTPUT}/pellet-panel*.csv | head -n 1) ${PROJ_HOME}/panel_pellet.csv
cp $(ls -t ${OUTPUT}/state-panel*.csv | head -n 1) ${PROJ_HOME}/panel_state.csv
cat ${PROJ_HOME}/attributes.csv | sed 's/ (.*)//g' | sed 's/, in /,/g' | sed 's/, on /,/g' | sed 's/Aboveground and belowground carbon/Carbon/g' | tr -d \" > ${PROJ_HOME}/att_unit.csv

echo ============ Data analysis with R ========== $(hostname) $(date)

## Run the model
Rscript panel_regression.R > Routputs.out
