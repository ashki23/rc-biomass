#!/bin/bash

## Uncomment the following to refresh the pellet data
# mv ${PROJ_HOME}/pellet_info.csv ${PROJ_HOME}/pellet_info_$(date +%Y%M%d).csv

cd ${PELLET}
wget -c --tries=2 http://biomassmagazine.com/plants/listplants/pellet/US/ -O pellet_list.html
cat pellet_list.html | grep -Po '(?<=/plants/view/)\d{4}' > pellet_url.txt
for l in $(cat pellet_url.txt); do wget -c -nv --tries=2 --random-wait http://biomassmagazine.com/plants/view/$l -O ${PELLET}/url/$l.html; done

cat url/*.html | grep -Po '(?<=<h3> ).*(?=<)' | tr -d , > name.txt
cat url/*.html | grep -Po '(?<=Capacity: </strong>).*(?=<)' | tr -d , > capacity.txt
cat url/*.html | grep -Po '(?<=Feedstock: </strong>).*(?=<)' > feedstock.txt
cat url/*.html | grep -Po '(?<=Location: </strong>).*(?= )' > city.txt
cat url/*.html | grep -Po '(?<=Location: </strong>).*(?=<)' | grep -Po '.{2}$' > state.txt
cat url/*.html | grep -Po '(?<=LatLng\().*(?=\))' | tr -d + > latlon.txt
cat url/*.html | grep -Po '(?<=Plant Status: </strong>).*(?=<)' > status.txt 
grep -n 'IDLE' name.txt | grep -Po '^.*(?=:)' > idle.txt
for l in $(cat idle.txt); do sed -i ""$l"s/.*/Idle/" status.txt; done 
for l in $(seq 1 $(cat name.txt | wc -l)); do echo $((1995 + RANDOM % 21)); done > opened.txt
for l in $(seq 1 $(cat name.txt | wc -l)); do echo $(date +%Y); done > retired.txt
for l in $(cat idle.txt); do sed -i ""$l"s/.*/$((($(date +%Y) - RANDOM % 3) -1))/" retired.txt; done 

paste -d , name.txt feedstock.txt capacity.txt city.txt state.txt latlon.txt status.txt opened.txt retired.txt > pellet_all.txt
sed -i '1 i\name,feedstock,capacity,city,state,lat,lon,status,opened,retired' pellet_all.txt

cd ${PROJ_HOME}
cat ${PELLET}/pellet_all.txt | grep -Pwv 'null|N/A|TBD|Ag|NL' > pellet_info.csv
