#!/bin/bash

cd ${OTHER}
wget -c -nv --tries=2 https://www.srs.fs.usda.gov/econ/data/mills/mill2005p.xls -O ./mill2005.xls
wget -c -nv --tries=2 https://apps.fs.usda.gov/fia/datamart/CSV/REF_POP_ATTRIBUTE.csv -O ./attributes_all.csv
wget -c -nv --tries=2 https://osav-usdot.opendata.arcgis.com/datasets/f682e9a104884a9385630ab846cbf873_0.zip -O ./major_ports_usdot.zip
wget -c -nv --tries=2 https://www2.census.gov/programs-surveys/popest/datasets/2000-2009/counties/totals/co-est2009-alldata.csv
wget -c -nv --tries=2 https://www2.census.gov/programs-surveys/popest/datasets/2010-2018/counties/totals/co-est2018-alldata.csv
wget -c -nv --tries=2 https://www2.census.gov/geo/tiger/GENZ2018/shp/cb_2018_us_county_5m.zip
wget -c -nv --tries=2 https://www2.census.gov/geo/tiger/GENZ2018/shp/cb_2018_us_state_20m.zip
wget -c -nv --tries=2 https://www2.census.gov/geo/docs/reference/state.txt
wget -c -nv --tries=2 https://people.ohio.edu/dyer/dyer_forestregions.zip
wget -c -nv --tries=2 https://gitlab.com/ashki23/backup-biomass/-/raw/main/pulp_info.csv
wget -c -nv --tries=2 https://gitlab.com/ashki23/backup-biomass/-/raw/main/other_data/pellet_data/pellet_info.csv

sleep 5
unzip -n ./major_ports_usdot.zip -d ./major_ports
unzip -n ./cb*state*zip -d ./shape_state
unzip -n ./cb*county*zip -d ./shape_county
unzip -n ./*forestregions.zip -d ./forest_regions
iconv -c -f utf-8 -t utf-8 ./co-est2009* > ./pop_2000_2009.csv
iconv -c -f utf-8 -t utf-8 ./co-est2018* > ./pop_2010_2018.csv

mv ./pulp_info.csv ${PROJ_HOME}
cat state.txt | awk -F '|' '{print $3","$2}' | tail -n +2 > ${PROJ_HOME}/state_abb.csv
cat state.txt | awk -F '|' '{print $2","$1}' | tail -n +2 > ${PROJ_HOME}/state_codes.csv

cd ${PROJ_HOME}
