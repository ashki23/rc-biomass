#!/bin/bash

export PROJ_HOME=${PWD}
export OUTPUT=${PWD}/outputs
export PELLET=${PWD}/pellet_plants
export OTHER=${PWD}/other_data
export FIA=${PWD}/fia_data
install -dvp ${OUTPUT}
install -dvp ${PELLET}/url
install -dvp ${OTHER}/power_plants
install -dvp ${FIA}/survey
MYHOME=${HOME}

## Install Miniconda
if [ ! -d miniconda ]; then 
source bootstrap.sh
fi

## Initiate conda
export HOME=${PROJ_HOME}
export PATH=${PROJ_HOME}/miniconda/bin/:${PATH}
source ./miniconda/etc/profile.d/conda.sh

## Deactivat active envs
conda deactivate

echo ============ Create Conda envs ============= $(hostname) $(date) 

## Create local environments
if [ ! -d bio_py_env ]; then
## Including: python openpyxl xlrd jq shapely fiona
conda create --yes --prefix ./bio_py_env --file ./bio_py_env.txt
fi

if [ ! -d bio_r_env ]; then
## Including: r-base r-sf=0.9_6 r-lwgeom=0.1_7 r-plm r-rjson r-knitr r-codetools r-spdep r-spam r-coda
conda create --yes --prefix ./bio_r_env --channel conda-forge --file ./bio_r_env.txt
## The current lwgeom needs libproj.so.15
ln -s ${PROJ_HOME}/bio_r_env/lib/libproj.so ${PROJ_HOME}/bio_r_env/lib/libproj.so.15
fi

## Activate Python environment
conda activate ./bio_py_env
export HOME=${MYHOME}
